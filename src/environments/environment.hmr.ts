export const environment = {
    SERVER_URL: `./`,
    production: true,
    hmr: true,
    SERVICE_URL: 'http://localhost:1000'
};
