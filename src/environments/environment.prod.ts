export const environment = {
    SERVER_URL: `./`,
    production: true,
    hmr: false,
    SERVICE_URL: 'http://localhost:1000'
};
