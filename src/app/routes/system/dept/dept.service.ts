import {Injectable} from '@angular/core';
import {_HttpClient} from "@core/services/http.client";

@Injectable()
export class DeptService {

    url = this.http.API_URL + '/system/dept';

    constructor(private http: _HttpClient) {
    }

    getTree() {
        return this.http.get(this.url + '/tree');
    }

    getList(id) {
        return this.http.get(this.url + '/list/' + id);
    }

    del(id: any) {
        return this.http.delete(this.url + '/' + id);
    }

    save(item) {
        return this.http.post(this.url, item);
    }
}
