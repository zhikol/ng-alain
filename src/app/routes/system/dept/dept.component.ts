import { Component, OnInit } from '@angular/core';
import {DeptService} from "./dept.service";
import {NzMessageService} from "ng-zorro-antd";
import {ModalHelper} from "@shared/helper/modal.helper";
import {DeptEditComponent} from "./dept-edit/dept-edit.component";
import {ACLService} from "@core/acl/acl.service";

@Component({
  selector: 'app-dept',
  templateUrl: './dept.component.html',
  styleUrls: ['./dept.component.scss']
})
export class DeptComponent implements OnInit {

    data = [];
    expandDataCache = {};

    collapse(array, data, $event) {
        if ($event === false) {
            if (data.children) {
                data.children.forEach(d => {
                    const target = array.find(a => a.id === d.id);
                    target.expand = false;
                    this.collapse(array, target, false);
                });
            } else {
                return;
            }
        }
    }

    convertTreeToList(root) {
        const stack = [], array = [], hashMap = {};
        stack.push({ ...root, level: 0, expand: true });

        while (stack.length !== 0) {
            const node = stack.pop();
            this.visitNode(node, hashMap, array);
            if (node.children) {
                for (let i = node.children.length - 1; i >= 0; i--) {
                    stack.push({ ...node.children[ i ], level: node.level + 1, expand: true, parent: node });
                }
            }
        }
        return array;
    }

    visitNode(node, hashMap, array) {
        if (!hashMap[ node.id ]) {
            hashMap[ node.id ] = true;
            array.push(node);
        }
    }

    constructor(
                public acl: ACLService,
                private deptService: DeptService,
                private nzMessage: NzMessageService,
                private modalHelper: ModalHelper) {
    }

    edit(i) {
        this.modalHelper.static(DeptEditComponent, { i }, '').subscribe(() => {
            this.load();
            // this.nzMessage.info('回调，重新发起列表刷新');
        });
    }

    del(id) {
        this.deptService.del(id).subscribe(() => {
            this.load();
            this.nzMessage.success("删除成功");
        });
    }

    load() {
        this.deptService.getTree().subscribe(data => {
            this.data = data;
            this.data.forEach(item => {
                this.expandDataCache[ item.id ] = this.convertTreeToList(item);
            });
        });
    }

    ngOnInit() {
        this.load();
    }
}
