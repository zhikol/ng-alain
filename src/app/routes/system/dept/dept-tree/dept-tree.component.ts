import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DeptService} from "../dept.service";

@Component({
    selector: 'app-dept-tree',
    templateUrl: './dept-tree.component.html',
    styleUrls: ['./dept-tree.component.scss']
})
export class DeptTreeComponent implements OnInit {

    @Output() change: EventEmitter<number> = new EventEmitter<number>();

    nodes = [];
    activateId;
    previousEvent;

    load() {
        this.srv.getTree().subscribe(data => {
            this.nodes = data;

        });
    }

    onEvent(ev: any) {
        if (this.previousEvent === 'activate' ||
            this.previousEvent === 'deactivate') {
            if (ev.focusedNodeId) {
                if (ev.focusedNodeId === this.activateId) {
                    this.activateId = null;
                } else {
                    this.activateId = ev.focusedNodeId;
                }
                this.change.emit(this.activateId);
            }
        }
        this.previousEvent = ev.eventName;
    }

    constructor(private srv: DeptService) {
    }

    ngOnInit() {
        this.load();
    }

}
