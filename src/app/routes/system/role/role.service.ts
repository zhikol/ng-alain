import { Injectable } from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {_HttpClient} from "@core/services/http.client";

@Injectable()
export class RoleService {

    url = this.http.API_URL + '/system/role';

    constructor(private http: _HttpClient) {
    }

    get(deptId) {
        if (!deptId) {
            deptId = '';
        }
        const params = {deptId: deptId};
        return this.http.get(this.url, params);
    }

    del(id: any) {
        return this.http.delete(this.url + '/' + id);
    }

    save(item) {
        return this.http.post(this.url, item);
    }

    grant(roleResInfo) {
        return this.http.post(this.url + '/grant', roleResInfo);
    }

    getResIds(id: any) {
        return this.http.get(this.url + '/resourceIds/' + id);
    }
}
