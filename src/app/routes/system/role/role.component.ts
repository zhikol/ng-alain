import {Component, OnInit} from '@angular/core';
import {NzMessageService} from "ng-zorro-antd";
import {ModalHelper} from "@shared/helper/modal.helper";
import {RoleService} from "./role.service";
import {RoleEditComponent} from "./role-edit/role-edit.component";
import {RoleResourceComponent} from "./role-resource/role-resource.component";
import {ACLService} from "@core/acl/acl.service";

@Component({
    selector: 'app-role',
    templateUrl: './role.component.html',
    styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

    deptId;

    change(deptId) {
        this.deptId = deptId;
        this.load();
    }

    data = [];

    constructor(public srv: RoleService,
                public acl: ACLService,
                public msgSrv: NzMessageService,
                private modalHelper: ModalHelper) {
    }

    ngOnInit() {
        this.load();
    }

    load() {
        this.srv.get(this.deptId).subscribe(data => {
            this.data = data;
        });
    }

    edit(i) {
        this.modalHelper.static(RoleEditComponent, { i }, '').subscribe(() => {
            this.load();
        });
    }

    grant(id) {
        this.modalHelper.static(RoleResourceComponent, { id }, '').subscribe(() => {
            this.load();
        });
    }

    del(id) {
        this.srv.del(id).subscribe(() => {
            this.load();
            this.msgSrv.success('删除成功');
        });
    }
}
