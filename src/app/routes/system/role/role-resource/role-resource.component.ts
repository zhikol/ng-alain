import {Component, OnInit} from '@angular/core';
import {ResourceService} from "../../resource/resource.service";
import {NzMessageService, NzModalSubject} from "ng-zorro-antd";
import {RoleService} from "../role.service";

class RoleResInfo {
    roleId;
    resourceIds;
}

@Component({
    selector: 'app-role-resource',
    templateUrl: './role-resource.component.html',
    styleUrls: ['./role-resource.component.scss']
})
export class RoleResourceComponent implements OnInit {

    id;
    nodes = [];
    roleResInfo = new RoleResInfo();
    resIds;
    chgChecked = false;

    constructor(private subject: NzModalSubject,
                private nzMessage: NzMessageService,
                private resSrv: ResourceService,
                private roleSrv: RoleService) {
    }

    ngOnInit() {
        this.load();
    }

    load() {
        this.roleResInfo.roleId = this.id;
        this.roleSrv.getResIds(this.id).subscribe(data => {
            this.resIds = new Set(data);
            this.roleResInfo.resourceIds = [];
            this.resIds.forEach(k => this.roleResInfo.resourceIds.push(k));
            this.resSrv.getTree().subscribe(d => {
                this._addCheckedField(d);
                this.nodes = d;
            });
        });
    }

    _addCheckedField(list) {
        let hasChecked = false;
        let hasUnchecked = false;
        let hasHalfChecked = false;
        list.forEach((val, idx, array) => {
            if (val.children) { // 有儿子
                const ch = this._addCheckedField(val.children);
                if (ch === 'checked') {
                    val.checked = true;
                    hasChecked = true;
                } else if (ch === 'halfChecked') {
                    val.halfChecked = true;
                    hasHalfChecked = true;
                } else {
                    hasUnchecked = true;
                }
            } else { // 叶子节点
                if (this.resIds.has(val.id)) {
                    val.checked = true;
                    hasChecked = true;
                } else {
                    hasUnchecked = true;
                }
            }
        });
        if (hasChecked) {
            if (hasUnchecked || hasHalfChecked) {
                return 'halfChecked';
            } else {
                return 'checked';
            }
        } else {
            if (hasHalfChecked) {
                return 'halfChecked';
            }
        }
    }

    _chgCheck(node) {
        this.chgChecked = true;
        node.forEach((val, idx, array) => {
            if (val.checked || val.halfChecked) {
                this.resIds.add(val.id);
            }
            if (val.children) { // 有儿子
                this._chgCheck(val.children);
            }
        });

    }

    onCheck(ev: any) {
        this.resIds.clear();
        this._chgCheck(ev.node.treeModel.nodes);
        this.roleResInfo.resourceIds = [];
        this.resIds.forEach(k => this.roleResInfo.resourceIds.push(k));
    }

    save() {
        this.roleSrv.grant(this.roleResInfo).subscribe(() => {
            this.nzMessage.success('保存成功');
            this.subject.next(true);
            this.close();
        });
    }

    close() {
        this.subject.destroy();
    }
}
