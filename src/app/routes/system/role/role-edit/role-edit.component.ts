import { Component, OnInit } from '@angular/core';
import {NzMessageService, NzModalSubject} from "ng-zorro-antd";
import {DeptService} from "../../dept/dept.service";
import {RoleService} from "../role.service";

class Role {
    id;
    name;
    deptid;
    enable;
    remark;
}

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss']
})
export class RoleEditComponent implements OnInit {

    i;

    _item: Role = new Role();
    _options = null;
    _loaded = false;
    _value: any[] = [];

    _changeValue(value) {
        this._item.deptid = this._value[this._value.length - 1];
    }

    _addIsLeafField(list) {
        list.forEach((val, idx, array) => {
            if (val.children) {
                this._addIsLeafField(val.children);
            } else {
                val.isLeaf = true;
            }
        });
    }

    _setParent(cur) {
        if (cur.id !== 0) {
            this.service.getList(cur.deptid).subscribe(data => {
                this._value = data;
            });
        }
    }

    save() {
        this.srv.save(this._item).subscribe(() => {
            this.nzMessage.success('保存成功');
            this.subject.next(true);
            this.close();
        });
    }

    close() {
        this.subject.destroy();
    }

    constructor(private subject: NzModalSubject,
                private nzMessage: NzMessageService,
                private service: DeptService,
                private srv: RoleService) {
    }

    ngOnInit() {
        this._setParent(this.i);
        this._item.id = this.i.id;
        if (this._item.id > 0) {
            this._item.name = this.i.name;
            this._item.enable = this.i.enable;
            this._item.deptid = this.i.deptid;
            this._item.remark = this.i.remark;
        } else {
            this._item.enable = false;
        }
        this.service.getTree().subscribe(data => {
            this._addIsLeafField(data);
            this._options = data;
            this._loaded = true;
        });
    }
}
