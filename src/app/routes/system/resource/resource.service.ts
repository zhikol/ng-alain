import { Injectable } from '@angular/core';
import {_HttpClient} from "@core/services/http.client";

@Injectable()
export class ResourceService {

    url = this.http.API_URL + '/system/resource';

    getTree() {
        return this.http.get(this.url + '/tree');
    }

    getMenuTree() {
        return this.http.get(this.url + '/menuTree');
    }

    save(item) {
        return this.http.post(this.url, item);
    }

    constructor(private http: _HttpClient) {
    }

    del(id: any) {
        return this.http.delete(this.url + '/' + id);
    }
}
