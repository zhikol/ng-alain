import { Injectable } from '@angular/core';
import {_HttpClient} from "@core/services/http.client";

@Injectable()
export class UserService {

    url = this.http.API_URL + '/system/user';

    constructor(private http: _HttpClient) {
    }

    get(args) {
        // let param = new HttpParams();
        // Object.keys(args).forEach(key => {
        //     param = param.append(key, args[key]);
        // });
        return this.http.get(this.url, args);
    }

    del(id: any) {
        return this.http.delete(this.url + '/' + id);
    }

    save(item) {
        return this.http.post(this.url, item);
    }
}
