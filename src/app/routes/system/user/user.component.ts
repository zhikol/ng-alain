import { Component, OnInit } from '@angular/core';
import {NzMessageService} from "ng-zorro-antd";
import {ModalHelper} from "@shared/helper/modal.helper";
import {UserService} from "./user.service";
import {UserEditComponent} from "./user-edit/user-edit.component";
import {ACLService} from "@core/acl/acl.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

    _total = 1;
    _dataSet = [];
    _loading = true;

    args: any = {
        page: 1,
        size: 10,
        sex: ''
    };

    constructor(
                public acl: ACLService,
                public srv: UserService,
                public msgSrv: NzMessageService,
                private modalHelper: ModalHelper) {
    }

    clear() {
        this.args = {
            page: 1,
            size: 10,
            sex: ''
        };
        this.load();
    }

    load(reset = false) {
        if (reset) {
            this.args.page = 1;
        }
        this._loading = true;
        this.srv.get(this.args).subscribe(data => {
            this._loading = false;
            this._total = data.totalElements;
            this._dataSet = data.content;
        });
    };

    ngOnInit() {
        this.load();
    }


    edit(i) {
        this.modalHelper.static(UserEditComponent, { i }, '').subscribe(() => {
            this.load();
        });
    }

    del(id) {
        this.srv.del(id).subscribe(() => {
            this.load(true);
            this.msgSrv.success('删除成功');
        });
    }

}
