import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import { ResourceComponent } from './resource/resource.component';
import {SharedModule} from "@shared/shared.module";
import { ResourceEditComponent } from './resource/resource-edit/resource-edit.component';
import { DeptComponent } from './dept/dept.component';
import {DeptService} from "./dept/dept.service";
import {ResourceService} from "./resource/resource.service";
import { DeptEditComponent } from './dept/dept-edit/dept-edit.component';
import { RoleComponent } from './role/role.component';
import {NzTreeModule} from "ng-tree-antd";
import {RoleService} from "./role/role.service";
import { DeptTreeComponent } from './dept/dept-tree/dept-tree.component';
import { RoleEditComponent } from './role/role-edit/role-edit.component';
import { RoleResourceComponent } from './role/role-resource/role-resource.component';
import { UserComponent } from './user/user.component';
import {UserService} from "./user/user.service";
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { DocComponent } from './doc/doc.component';

const routes: Routes = [
    { path: 'user', component: UserComponent },
    { path: 'role', component: RoleComponent },
    { path: 'dept', component: DeptComponent },
    { path: 'resource', component: ResourceComponent },
    { path: 'doc', component: DocComponent },
];

const COMPONENTS_NOROUNT = [ ResourceEditComponent, DeptEditComponent, RoleEditComponent, RoleResourceComponent, UserEditComponent ];

@NgModule({
  imports: [
      SharedModule,
      RouterModule.forChild(routes),
      NzTreeModule
  ],
  providers: [ResourceService, DeptService, RoleService, UserService],
  declarations: [...COMPONENTS_NOROUNT, ResourceComponent, DeptComponent, RoleComponent, DeptTreeComponent, UserComponent, DocComponent],
    entryComponents: COMPONENTS_NOROUNT
})
export class SystemModule { }
