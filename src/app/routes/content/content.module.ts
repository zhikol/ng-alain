import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "@shared/shared.module";
import { NewsComponent } from './news/news.component';
import { AdComponent } from './ad/ad.component';
import {NewsService} from "./news/news.service";

const routes: Routes = [
    { path: 'ad', component: AdComponent },
    { path: 'news', component: NewsComponent }
];


@NgModule({
  imports: [
      SharedModule,
      RouterModule.forChild(routes)
  ],
  declarations: [NewsComponent, AdComponent],
    providers: [NewsService]
})
export class ContentModule { }
