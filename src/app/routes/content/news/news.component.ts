import {Component, OnInit} from '@angular/core';
import {NzMessageService} from "ng-zorro-antd";
import {ACLService} from "@core/acl/acl.service";
import {NewsService} from "./news.service";

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

    _total = 1;
    _dataSet = [];
    _loading = true;

    args: any = {
        page: 1,
        size: 10,
        category: ''
    };

    constructor(public acl: ACLService,
                public srv: NewsService,
                public msgSrv: NzMessageService) {
    }

    clear() {
        this.args = {
            page: 1,
            size: 10,
            category: ''
        };
        this.load();
    }

    load(reset = false) {
        if (reset) {
            this.args.page = 1;
        }
        this._loading = true;
        this.srv.get(this.args).subscribe(data => {
            this._loading = false;
            this._total = data.totalElements;
            this._dataSet = data.content;
        });
    };

    ngOnInit() {
        this.load();
    }

    del(id) {
        this.srv.del(id).subscribe(() => {
            this.load(true);
            this.msgSrv.success('删除成功');
        });
    }

}
