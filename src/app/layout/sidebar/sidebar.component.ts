import { Component } from '@angular/core';
import { NzMessageService } from "ng-zorro-antd";
import { SettingsService } from "@core/services/settings.service";
import { MenuService } from '@core/services/menu.service';
import {_HttpClient} from "@core/services/http.client";
import {environment} from "../../../environments/environment";
import {Router} from "@angular/router";

@Component({
  selector   : 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls  : ['./sidebar.component.scss']
})
export class SidebarComponent {
    constructor(
        private http: _HttpClient,
        private router: Router,
        private _message: NzMessageService,
        public menuSrv: MenuService,
        public settings: SettingsService) {
    }

    show(msg: string) {
        this._message.success(msg);
    }

    closeMenu() {
        this.settings.setLayout('collapsed', false);
    }

    logout() {
        const url = environment.SERVICE_URL + '/logout';
        this.http.post(url).subscribe(() => {
            this.settings.setUser(null);
            this.router.navigate(['/login']);
        });
    }
}
